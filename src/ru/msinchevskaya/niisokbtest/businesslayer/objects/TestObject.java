package ru.msinchevskaya.niisokbtest.businesslayer.objects;

import org.json.JSONObject;

import ru.msinchevskaya.niisokbtest.FieldNames;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Мария on 29.10.2014.
 */
public class TestObject implements Parcelable{

	public static final String TAG = "businesslayer.objects.TestObject";
	
    public final int id;
    public final String title;
    public final String img;

    public TestObject(JSONObject json){

        id = json.optInt(FieldNames.ID);
        title = json.optString(FieldNames.TITLE);
        img = json.optString(FieldNames.IMG);

    }

    public TestObject(Parcel parcel){

        id = parcel.readInt();
        title = parcel.readString();
        img = parcel.readString();
    }
    
    public int getId(){
    	return id;
    }
    
    public String getTitle(){
    	return title;
    }
    
    public String getImg(){
    	return img;
    }
    
    @Override
    public String toString(){
    	return title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(title);
        parcel.writeString(img);
    }

    public static final Creator<TestObject> CREATOR = new Creator<TestObject>() {
        @Override
        public TestObject createFromParcel(Parcel parcel) {
            return new TestObject(parcel);
        }

        @Override
        public TestObject[] newArray(int i) {
            return new TestObject[i];
        }
    };
}
