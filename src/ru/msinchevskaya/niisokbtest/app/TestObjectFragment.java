package ru.msinchevskaya.niisokbtest.app;

import com.nostra13.universalimageloader.core.ImageLoader;

import ru.msinchevskaya.niisokbtest.R;
import ru.msinchevskaya.niisokbtest.businesslayer.objects.TestObject;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ImageView;

public class TestObjectFragment extends Fragment {
	
	public static final String TAG = "app.TestObjectFragment";
	
	private TestObject mObject;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		mObject = getArguments().getParcelable(SecondActivity.EXTRA_TEST_OBJECT);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		final View contentView = inflater.inflate(R.layout.fragment_test_object, null);
		((TextView) contentView.findViewById(R.id.tvTitle)).setText(mObject.getTitle());
		
		ImageLoader.getInstance().displayImage(mObject.getImg(), (ImageView) contentView.findViewById(R.id.ivImg));
		
		return contentView;
	}

}
