package ru.msinchevskaya.niisokbtest.app;
import ru.msinchevskaya.niisokbtest.datalayer.controllers.ImageController;
import android.app.Application;

public class NiiSokbTestApplication extends Application {

	public static final String TAG = "app.NiiSokbTestApplication";
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		ImageController.getInstance().initialize(getApplicationContext());
	}
}
