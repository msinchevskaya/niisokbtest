package ru.msinchevskaya.niisokbtest.app;

import java.util.ArrayList;

import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import ru.msinchevskaya.niisokbtest.R;
import ru.msinchevskaya.niisokbtest.businesslayer.objects.TestObject;
import ru.msinchevskaya.niisokbtest.networklayer.NetworkRequest;
import ru.msinchevskaya.niisokbtest.networklayer.NetworkService;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class FirstActivity extends FragmentActivity {
	
	public static final String TAG = "app.FirstActivity";
	public static final String EXTRA_TEST_OBJECTS = "extratestobjects";
	public static final String EXTRA_OBJECT_COUNTER = "extraobjectcounter";
	
	private final ArrayList<TestObject> objects = new ArrayList<TestObject>();
	
	private ListView mListView;
	private ArrayAdapter<TestObject> mAdapter;
	private ObjectLoadListener mLoadListener; 
	private View mProgress;
	
	private final SpiceManager mSpiceManager = new SpiceManager(NetworkService.class);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_first);
		

		mProgress = findViewById(R.id.progress);
		mProgress.setVisibility(View.GONE);
		
		mListView = (ListView) findViewById(R.id.lvObjects);
		
		mAdapter = new ArrayAdapter<TestObject>(getApplicationContext(), R.layout.row_test_object, objects);
		mListView.setAdapter(mAdapter);

		mListView.setOnItemClickListener(objectClickListener);
		
		mLoadListener = new ObjectLoadListener();
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		mSpiceManager.start(this);
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		mSpiceManager.shouldStop();
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		
		objects.clear();
		final ArrayList<TestObject> restore = savedInstanceState.getParcelableArrayList(EXTRA_TEST_OBJECTS);
		objects.addAll(restore);
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putParcelableArrayList(EXTRA_TEST_OBJECTS, objects);
	}
	
	public void onRefreshClick(View v){
		loadDataBegin();
	}
	
	private OnItemClickListener objectClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			startSecondActivity(position);
		}
	};
	
	private void startSecondActivity(int counter){
		Intent intent = new Intent(getApplicationContext(), SecondActivity.class);
		intent.putParcelableArrayListExtra(EXTRA_TEST_OBJECTS, objects);
		intent.putExtra(EXTRA_OBJECT_COUNTER, counter);
		startActivity(intent);
	}
	
	private void loadDataBegin(){
		mSpiceManager.execute(new NetworkRequest(), mLoadListener);
		mProgress.setVisibility(View.VISIBLE);
	}
	
	private class ObjectLoadListener implements RequestListener<TestObject[]> {

		@Override
		public void onRequestFailure(SpiceException exception) {
			Toast.makeText(getApplicationContext(), exception.getLocalizedMessage(), Toast.LENGTH_LONG).show();
			mProgress.setVisibility(View.GONE);
		}

		@Override
		public void onRequestSuccess(TestObject[] array) {
			objects.clear();
			
			for (int i = 0; i < array.length; i++){
				objects.add(array[i]);
			}
			
			mAdapter.notifyDataSetChanged();
			mProgress.setVisibility(View.GONE);
		}
		
	}
	

}
