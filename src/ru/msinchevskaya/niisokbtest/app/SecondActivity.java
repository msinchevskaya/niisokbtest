package ru.msinchevskaya.niisokbtest.app;

import java.util.ArrayList;

import ru.msinchevskaya.niisokbtest.R;
import ru.msinchevskaya.niisokbtest.businesslayer.objects.TestObject;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

public class SecondActivity extends FragmentActivity {
	
	public static final String TAG = "app.SecondActivity";
	
	public static final String EXTRA_TEST_OBJECT = "extratestobject";
	
	private PagerAdapter mAdapter;
	private ViewPager mPager;
	private ArrayList<TestObject> objects;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_second);
		
		objects = getIntent().getParcelableArrayListExtra(FirstActivity.EXTRA_TEST_OBJECTS);
		
		mPager = (ViewPager) findViewById(R.id.pager);
		
		mAdapter = new PagerAdapter(getSupportFragmentManager());
		mPager.setAdapter(mAdapter);
		mPager.setCurrentItem(getIntent().getIntExtra(FirstActivity.EXTRA_OBJECT_COUNTER, 0));
	}
	
	
	private class PagerAdapter extends FragmentPagerAdapter{

		public PagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int arg0) {
			final TestObjectFragment fragment = new TestObjectFragment();
			final Bundle bundle = new Bundle();
			bundle.putParcelable(EXTRA_TEST_OBJECT, objects.get(arg0));
			fragment.setArguments(bundle);
			return fragment;
		}

		@Override
		public int getCount() {
			return objects.size();
		}
		
		
	}
}
