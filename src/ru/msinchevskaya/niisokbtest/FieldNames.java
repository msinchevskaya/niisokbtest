package ru.msinchevskaya.niisokbtest;

/**
 * Created by Мария on 29.10.2014.
 */
public class FieldNames {

    private FieldNames(){}

    public static final String ID = "id";
    public static final String TITLE = "title";
    public static final String IMG = "img";
}
