package ru.msinchevskaya.niisokbtest.datalayer.controllers;

import android.content.Context;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

/**
 * Данный синглтон берет на себя функцию щагрузки изображения, в роли обертки над Universal Image Loader. Весь код по загрузке изображени, желательно хранить здесь
 */
public class ImageController {
	
	public static final String TAG = "datalayer.controllers.ImageController";

	private static final int THREAD_POOL_SIZE_DEFAULT = 2;

	private static volatile ImageController sInstance;

	public static ImageController getInstance() {
		ImageController instance = sInstance;
		if (instance == null) {
			synchronized (ImageController.class) {
				instance = sInstance;
				if (instance == null) {
					instance = sInstance = new ImageController();
				}
			}
		}

		return sInstance;
	}




	private ImageController() {

	}




	public void initialize(Context context) {
		final DisplayImageOptions options = new DisplayImageOptions.Builder()
		.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
		.cacheOnDisk(true)
		.cacheInMemory(true)
		.resetViewBeforeLoading(true)
		.build();

		final ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
		.threadPoolSize(THREAD_POOL_SIZE_DEFAULT)
		.defaultDisplayImageOptions(options)
		.denyCacheImageMultipleSizesInMemory()
		.tasksProcessingOrder(QueueProcessingType.LIFO)
		.build();

		ImageLoader.getInstance().init(config);
	}
}