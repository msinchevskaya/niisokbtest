package ru.msinchevskaya.niisokbtest.networklayer;

import org.json.JSONArray;
import org.json.JSONObject;

import ru.msinchevskaya.niisokbtest.businesslayer.objects.TestObject;

import com.octo.android.robospice.request.SpiceRequest;

public class NetworkRequest extends SpiceRequest<TestObject[]>{
	
	public static final String TAG = "networklayer.NetworkRequest";
	private static final String URL = "http://private-anon-d46f06ad5-jsontest111.apiary-mock.com/androids";

	public NetworkRequest() {
		super(TestObject[].class);
	}

	@Override
	public TestObject[] loadDataFromNetwork() throws Exception {
		
		final String response = NetworkDataAccessor.getAsString(URL);
		final JSONArray array = new JSONArray(response);

		final TestObject[] objects = new TestObject[array.length()];
		
		for (int i = 0; i < array.length(); i++){
			JSONObject json = array.getJSONObject(i);
			objects[i] = new TestObject(json);
		}
		return objects;
	}

}
