package ru.msinchevskaya.niisokbtest.networklayer;
import com.octo.android.robospice.UncachedSpiceService;

public class NetworkService extends UncachedSpiceService{
	
	public static final String TAG = "networklayer.NetworkService";

	private static final int THREAD_COUNT = 3;

	@Override
	public int getThreadCount() {
		return THREAD_COUNT;
	}
}
